import 'bootstrap/dist/css/bootstrap.css';
//import 'bootstrap-vue/dist/bootstrap-vue.css';
import bootstrap from 'bootstrap/dist/js/bootstrap.js'
import 'bootstrap';
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css';
import './assets/main.css';

import { createApp } from 'vue';
import { createPinia } from 'pinia';

import App from './App.vue';
import router from './router';

//const app = new Vue({ el: "#app", render: h =>(App)});
const app = createApp(App).use(bootstrap);

app.use(createPinia());
app.use(router);

app.mount('#app');
