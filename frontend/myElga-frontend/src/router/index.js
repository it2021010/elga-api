import { createRouter, createWebHistory, useRoute } from 'vue-router'
import { useApplicationStore } from '@/stores/application.js'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
        path: '/',
        name: 'home',
        component: () => import('../views/Public/HomeView.vue')
    },
    {
        path: '/:id/profile',
        name: 'farmer_profile',
        component: () => import('../views/Citizen/ProfileView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/:id/profile/edit',
        name: 'edit_profile',
        component: () => import('../views/Citizen/ProfileEditView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/:id/profile/applications',
        name: 'my_submissions',
        component: () => import('../views/Citizen/MyApplicationsView.vue'),
        meta: { requiresAuth: true },
    },
    {
        path: '/:id/profile/applications/submission/:submissionId',
        name: 'submission-info',
        component: () => import('../views/Citizen/MySubmissionView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/:id/profile/applications/submission/update/:submissionId',
        name: 'submission-update',
        component: () => import('../views/Citizen/MySubmissionEditView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/:id/profile/applications/breeder/:breederId',
        name: 'breeder-info',
        component: () => import('../views/Citizen/MyBreederView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/:id/profile/applications/breeder/:breederId/update',
        name: 'breeder-update',
        component: () => import('../views/Citizen/MyBreederEditView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/menu',
        name: 'submission_menu',
        component: () => import('../views/Citizen/ChooseSubmissionView.vue'),
        meta: { requiresAuth: false }
    },
    {
        path: '/menu/farmer-submission',
        name: 'farmer-submission',
        component: () => import('../views/Citizen/FormFarmerView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/menu/breeder-submission',
        name: 'breeder-submission',
        component: () => import('../views/Citizen/FormBreederView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/:id/manager',
        name: 'manager-profile',
        component: () => import('../views/Manager/ProfileView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/manage-applications',
        name: 'manager-catalog',
        component: () => import('../views/Manager/CatalogView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/manage-applications/submission/:submissionId',
        name: 'manager-submission',
        component: () => import('../views/Manager/SubmissionInfoView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/manage-applications/submission/:submissionId/update',
        name: 'manager-submission-update',
        component: () => import('../views/Manager/UpdateSubmissionView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/manage-applications/breeder/:breederId',
        name: 'manager-breeder',
        component: () => import('../views/Manager/BreederInfoView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/manage-applications/breeder/:breederId/update',
        name: 'manager-breeder-update',
        component: () => import('../views/Manager/UpdateBreederView.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../views/Public/LoginView.vue')
    }, 
    {
        path: '/signup',
        name: 'signup',
        component: () => import('../views/Public/SignupView.vue')
    },
    {
        path: '/signup/success',
        name: 'success-signup',
        component: () => import('../views/Public/SignUpSuccessView.vue')
    },
    {
        path: '/help',
        name: 'help',
        component: () => import('../views/Public/HelpView.vue')
    },
    {
        path: '/contact',
        name: 'contact',
        component: () => import('../views/Public/ContactView.vue')
    },
    {
        path: '/forgot-password',
        name: 'forgot-password',
        component: () => import('../views/Public/PasswordView.vue')
    },
    {
        path: '/forgot-password/change',
        name: 'change-password',
        component: () => import('../views/Public/PasswordChangeView.vue')
    },
    {
        path: '/verification/:email/:phoneNumber',
        name: 'verification',
        component: () => import('../views/Public/VerificationView.vue')
    },
    {
        path: '/:pathMatch(.*)*',
        component: () => import('../views/Public/PageNotFoundView.vue')
    },
    {
        path: '/unauthorized',
        name: 'unauthorized',
        component: () => import('../views/Public/UnauthorizedView.vue')
    },
    {
        path: '/admin-board',
        name: 'admin-board',
        component: () => import('../views/Admin/AdminBoard.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/create-manager',
        name: 'create-manager',
        component: () => import('../views/Admin/CreateManager.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/delete-user',
        name: 'delete-user',
        component: () => import('../views/Admin/DeleteUser.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/applications',
        name: 'all-applications',
        component: () => import('../views/Admin/AllApplications.vue'),
        meta: { requiresAuth: true }
    },
    {
        path: '/users',
        name: 'all-users',
        component: () => import('../views/Admin/AllUsers.vue'),
        meta: { requiresAuth: true }
    }
  ]
})

router.beforeEach((to, from, next) => {
    const { isAuthenticated, userData } = useApplicationStore();
    const requiresAuth = to.matched.some((record) => record.meta.requiresAuth);



    if (requiresAuth && !isAuthenticated) {
        console.log('user not authenticated. redirecting to /login');
        next('/login');
    }
    else if (to.matched.some((record) => record.meta.requiresAuth )) {
        if 
        (userData.roles == 'ROLE_CITIZEN' 
            && (
                to.name == 'farmer_profile' ||
                to.name == 'edit_profile' ||
                to.name == 'my_submissions' ||
                to.name == 'submission-info' ||
                to.name == 'submission-update' ||
                to.name == 'breeder-info' ||
                to.name == 'breeder-update' ||
                to.name == 'submission_menu' ||
                to.name == 'farmer-submission' ||
                to.name == 'breeder-submission'
                )
        ){
            next();
        } 
        else if 
        (userData.roles == 'ROLE_MANAGER'
            && (
                to.name == 'manager-profile' ||
                to.name == 'manager-catalog' || 
                to.name == 'manager-submission' ||
                to.name == 'manager-submission-update' ||
                to.name == 'manager-breeder' ||
                to.name == 'manager-breeder-update'
            )
        ){
            next();
        }
        else if
        (userData.roles == 'ROLE_ADMIN'
            && (
                to.name == 'admin-board' ||
                to.name == 'delete-user' ||
                to.name == 'create-manager' ||
                to.name == 'all-applications' ||
                to.name == 'all-users' || 
                to.name == 'manager-breeder' || 
                to.name == 'manager-submission' ||
                to.name == 'manager-breeder-update' || 
                to.name == 'manager-submission-update'
            )
        ){
            next();
        }
        else {
            next('/unauthorized');
        } 
    }
    else {
        next();
    }
});

export default router
