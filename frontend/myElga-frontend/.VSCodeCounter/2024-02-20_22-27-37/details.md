# Details

Date : 2024-02-20 22:27:37

Directory c:\\Users\\c_apo\\Desktop\\Backend myElga\\demo\\frontend\\myElga-frontend

Total : 59 files,  8584 codes, 6 comments, 403 blanks, all 8993 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [.eslintrc.cjs](/.eslintrc.cjs) | JavaScript | 12 | 1 | 2 | 15 |
| [.prettierrc.json](/.prettierrc.json) | JSON | 8 | 0 | 0 | 8 |
| [README.md](/README.md) | Markdown | 22 | 0 | 14 | 36 |
| [index.html](/index.html) | HTML | 15 | 0 | 1 | 16 |
| [jsconfig.json](/jsconfig.json) | JSON with Comments | 8 | 0 | 1 | 9 |
| [package-lock.json](/package-lock.json) | JSON | 3,815 | 0 | 1 | 3,816 |
| [package.json](/package.json) | JSON | 36 | 0 | 1 | 37 |
| [src/App.vue](/src/App.vue) | vue | 17 | 0 | 3 | 20 |
| [src/assets/base.css](/src/assets/base.css) | CSS | 119 | 0 | 13 | 132 |
| [src/assets/logo.svg](/src/assets/logo.svg) | XML | 1 | 0 | 1 | 2 |
| [src/assets/main.css](/src/assets/main.css) | CSS | 1 | 0 | 0 | 1 |
| [src/components/AppFooter.vue](/src/components/AppFooter.vue) | vue | 17 | 0 | 2 | 19 |
| [src/components/AppHeader.vue](/src/components/AppHeader.vue) | vue | 12 | 0 | 1 | 13 |
| [src/components/BreederInfo.vue](/src/components/BreederInfo.vue) | vue | 127 | 1 | 5 | 133 |
| [src/components/GoBackButton.vue](/src/components/GoBackButton.vue) | vue | 13 | 0 | 3 | 16 |
| [src/components/NavUpdateApplicManager.vue](/src/components/NavUpdateApplicManager.vue) | vue | 15 | 0 | 0 | 15 |
| [src/components/SubmissionInfo.vue](/src/components/SubmissionInfo.vue) | vue | 108 | 1 | 6 | 115 |
| [src/components/TableBreeder.vue](/src/components/TableBreeder.vue) | vue | 193 | 0 | 16 | 209 |
| [src/components/TableHeader.vue](/src/components/TableHeader.vue) | vue | 13 | 0 | 0 | 13 |
| [src/components/TableSubmissions.vue](/src/components/TableSubmissions.vue) | vue | 193 | 0 | 15 | 208 |
| [src/composables/fileDownloader.js](/src/composables/fileDownloader.js) | JavaScript | 53 | 0 | 9 | 62 |
| [src/composables/remotaDataAccess.js](/src/composables/remotaDataAccess.js) | JavaScript | 45 | 0 | 8 | 53 |
| [src/main.js](/src/main.js) | JavaScript | 13 | 2 | 6 | 21 |
| [src/router/index.js](/src/router/index.js) | JavaScript | 253 | 0 | 7 | 260 |
| [src/stores/application.js](/src/stores/application.js) | JavaScript | 49 | 0 | 5 | 54 |
| [src/views/Admin/AdminBoard.vue](/src/views/Admin/AdminBoard.vue) | vue | 75 | 0 | 5 | 80 |
| [src/views/Admin/AllApplications.vue](/src/views/Admin/AllApplications.vue) | vue | 30 | 0 | 10 | 40 |
| [src/views/Admin/AllUsers.vue](/src/views/Admin/AllUsers.vue) | vue | 60 | 0 | 7 | 67 |
| [src/views/Admin/CreateManager.vue](/src/views/Admin/CreateManager.vue) | vue | 226 | 0 | 19 | 245 |
| [src/views/Admin/DeleteUser.vue](/src/views/Admin/DeleteUser.vue) | vue | 85 | 0 | 10 | 95 |
| [src/views/Citizen/ChooseSubmissionView.vue](/src/views/Citizen/ChooseSubmissionView.vue) | vue | 92 | 0 | 5 | 97 |
| [src/views/Citizen/FormBreederView.vue](/src/views/Citizen/FormBreederView.vue) | vue | 175 | 0 | 8 | 183 |
| [src/views/Citizen/FormFarmerView.vue](/src/views/Citizen/FormFarmerView.vue) | vue | 163 | 0 | 8 | 171 |
| [src/views/Citizen/MyApplicationsView.vue](/src/views/Citizen/MyApplicationsView.vue) | vue | 42 | 0 | 7 | 49 |
| [src/views/Citizen/MyBreederEditView.vue](/src/views/Citizen/MyBreederEditView.vue) | vue | 181 | 0 | 9 | 190 |
| [src/views/Citizen/MyBreederView.vue](/src/views/Citizen/MyBreederView.vue) | vue | 31 | 0 | 4 | 35 |
| [src/views/Citizen/MySubmissionEditView.vue](/src/views/Citizen/MySubmissionEditView.vue) | vue | 168 | 0 | 13 | 181 |
| [src/views/Citizen/MySubmissionView.vue](/src/views/Citizen/MySubmissionView.vue) | vue | 31 | 0 | 5 | 36 |
| [src/views/Citizen/ProfileEditView.vue](/src/views/Citizen/ProfileEditView.vue) | vue | 132 | 0 | 11 | 143 |
| [src/views/Citizen/ProfileView.vue](/src/views/Citizen/ProfileView.vue) | vue | 119 | 0 | 10 | 129 |
| [src/views/Manager/BreederInfoView.vue](/src/views/Manager/BreederInfoView.vue) | vue | 27 | 0 | 4 | 31 |
| [src/views/Manager/CatalogView.vue](/src/views/Manager/CatalogView.vue) | vue | 39 | 0 | 10 | 49 |
| [src/views/Manager/ProfileEditView.vue](/src/views/Manager/ProfileEditView.vue) | vue | 116 | 0 | 8 | 124 |
| [src/views/Manager/ProfileView.vue](/src/views/Manager/ProfileView.vue) | vue | 86 | 0 | 9 | 95 |
| [src/views/Manager/SubmissionInfoView.vue](/src/views/Manager/SubmissionInfoView.vue) | vue | 27 | 0 | 3 | 30 |
| [src/views/Manager/UpdateBreederView.vue](/src/views/Manager/UpdateBreederView.vue) | vue | 152 | 0 | 12 | 164 |
| [src/views/Manager/UpdateSubmissionView.vue](/src/views/Manager/UpdateSubmissionView.vue) | vue | 150 | 0 | 12 | 162 |
| [src/views/Public/ContactView.vue](/src/views/Public/ContactView.vue) | vue | 42 | 0 | 1 | 43 |
| [src/views/Public/HelpView.vue](/src/views/Public/HelpView.vue) | vue | 80 | 0 | 5 | 85 |
| [src/views/Public/HomeView.vue](/src/views/Public/HomeView.vue) | vue | 144 | 0 | 9 | 153 |
| [src/views/Public/LoginView.vue](/src/views/Public/LoginView.vue) | vue | 150 | 0 | 12 | 162 |
| [src/views/Public/PageNotFoundView.vue](/src/views/Public/PageNotFoundView.vue) | vue | 14 | 0 | 0 | 14 |
| [src/views/Public/PasswordChangeView.vue](/src/views/Public/PasswordChangeView.vue) | vue | 156 | 0 | 15 | 171 |
| [src/views/Public/PasswordView.vue](/src/views/Public/PasswordView.vue) | vue | 139 | 0 | 13 | 152 |
| [src/views/Public/SignUpSuccessView.vue](/src/views/Public/SignUpSuccessView.vue) | vue | 39 | 0 | 3 | 42 |
| [src/views/Public/SignupView.vue](/src/views/Public/SignupView.vue) | vue | 321 | 0 | 18 | 339 |
| [src/views/Public/UnauthorizedView.vue](/src/views/Public/UnauthorizedView.vue) | vue | 21 | 0 | 0 | 21 |
| [src/views/Public/VerificationView.vue](/src/views/Public/VerificationView.vue) | vue | 100 | 0 | 14 | 114 |
| [vite.config.js](/vite.config.js) | JavaScript | 13 | 1 | 4 | 18 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)