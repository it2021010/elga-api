# Summary

Date : 2024-02-20 22:27:37

Directory c:\\Users\\c_apo\\Desktop\\Backend myElga\\demo\\frontend\\myElga-frontend

Total : 59 files,  8584 codes, 6 comments, 403 blanks, all 8993 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| vue | 43 | 4,121 | 2 | 330 | 4,453 |
| JSON | 3 | 3,859 | 0 | 2 | 3,861 |
| JavaScript | 7 | 438 | 4 | 41 | 483 |
| CSS | 2 | 120 | 0 | 13 | 133 |
| Markdown | 1 | 22 | 0 | 14 | 36 |
| HTML | 1 | 15 | 0 | 1 | 16 |
| JSON with Comments | 1 | 8 | 0 | 1 | 9 |
| XML | 1 | 1 | 0 | 1 | 2 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 59 | 8,584 | 6 | 403 | 8,993 |
| . (Files) | 8 | 3,929 | 2 | 24 | 3,955 |
| src | 51 | 4,655 | 4 | 379 | 5,038 |
| src (Files) | 2 | 30 | 2 | 9 | 41 |
| src\\assets | 3 | 121 | 0 | 14 | 135 |
| src\\components | 9 | 691 | 2 | 48 | 741 |
| src\\composables | 2 | 98 | 0 | 17 | 115 |
| src\\router | 1 | 253 | 0 | 7 | 260 |
| src\\stores | 1 | 49 | 0 | 5 | 54 |
| src\\views | 33 | 3,413 | 0 | 279 | 3,692 |
| src\\views\\Admin | 5 | 476 | 0 | 51 | 527 |
| src\\views\\Citizen | 10 | 1,134 | 0 | 80 | 1,214 |
| src\\views\\Manager | 7 | 597 | 0 | 58 | 655 |
| src\\views\\Public | 11 | 1,206 | 0 | 90 | 1,296 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)