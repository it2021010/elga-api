# myElga-frontend

This project is the frontend for myElga platform that communicates with rest api myElga root folder. Build with vue 3 js and bootstrap V5 framework.

## Run Application

<kbd>npm run dev</kbd>

## Accessing platform

Runs in localhost, port 5173.
