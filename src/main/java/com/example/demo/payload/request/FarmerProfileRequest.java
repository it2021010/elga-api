package com.example.demo.payload.request;

public class FarmerProfileRequest {

    private String address;
    private String phoneNumber;

    public FarmerProfileRequest(String address, String phoneNumber) {
        this.address = address;
        this.phoneNumber = phoneNumber;
    }
    public FarmerProfileRequest(){}

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
