package com.example.demo.payload.request;

public class PhoneEmailRequest {
    private String phoneNumber;
    private String email;

    public PhoneEmailRequest() {}
    public PhoneEmailRequest(String phoneNumber, String email) {
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
