package com.example.demo.repository;

import com.example.demo.entity.BreederSubmission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BreederSubmissionRepository extends JpaRepository<BreederSubmission, Long> {
}
