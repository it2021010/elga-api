package com.example.demo.repository;

import com.example.demo.entity.Farmer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@RepositoryRestResource(path = "farmer")
public interface FarmerRepository extends JpaRepository<Farmer, Long>{

}
