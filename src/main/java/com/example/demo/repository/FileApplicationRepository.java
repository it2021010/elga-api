package com.example.demo.repository;

import com.example.demo.entity.FileApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileApplicationRepository extends JpaRepository<FileApplication, Long> {
}
