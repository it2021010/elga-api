package com.example.demo.controller;

import com.example.demo.entity.*;
import com.example.demo.payload.request.ManageBreeder;
import com.example.demo.payload.request.ManageSubmission;
import com.example.demo.payload.request.ManagerRequest;
import com.example.demo.payload.request.RegisterManagerRequest;
import com.example.demo.payload.response.BreedersResponse;
import com.example.demo.payload.response.ManagerResponse;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.payload.response.SubmissionsResponse;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.BreederSubmissionService;
import com.example.demo.service.ManagerService;
import com.example.demo.service.SubmissionService;
import com.example.demo.service.UserService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/services/manager")
public class ElgaController {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ManagerService managerService;
    @Autowired
    private UserService userService;
    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private BreederSubmissionService breederSubmissionService;
    @Value("${twilio.account_sid}")
    private String accountSID;
    @Value("${twilio.auth_token}")
    private String authToken;


    @GetMapping("/submissions")
    public ResponseEntity<?> getAllSubmissions() {
        List<Submission> submissions = submissionService.getAllSubmissions();
        List<SubmissionsResponse> submissionList = new ArrayList<>();
        for (Submission s : submissions) {
            Double damages;
            if(s.getDamages() == null) {
                damages = (double) -1;
            }else {
                damages = s.getDamages().getDamagesSum();
            }
            submissionList.add(new SubmissionsResponse(s.getId(), s.getFieldArea(),
                    s.getAverageProduction(), s.getPricePerUnit(),
                    s.getDescription(), s.getDamagePercentage(),
                    s.getState(), s.getCreationDate(), damages));
        }
        return ResponseEntity.ok(submissionList);
    }
    @GetMapping("/breeders")
    public ResponseEntity<?> getAllBreeders() {
        List<BreederSubmission> breederSubmissions = breederSubmissionService.getAllSubmissions();
        List<BreedersResponse> breedersResponseList = new ArrayList<>();
        for (BreederSubmission b : breederSubmissions) {
            Double damages;
            if(b.getDamages() == null) {
                damages = (double) -1;
            }else {
                damages = b.getDamages().getDamagesSum();
            }
            breedersResponseList.add(new BreedersResponse(b.getId(), b.getCreationDate(),
                    b.getTotalAnimals(), b.getState(), b.getResidualValue(),
                    b.getPricePerUnit(), b.getNumberOfKilledAnimals(),
                    b.getDamageCoveragePercentage(), b.getCompensationFactor(),
                    damages));
        }
        return ResponseEntity.ok(breedersResponseList);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getManager(@PathVariable Long id) {
        ElgaManager manager = managerService.getManager(userService.getUserById(id).getManager().getId());

        return ResponseEntity.ok(new ManagerResponse(
                manager.getId(),
                manager.getFirstName(),
                manager.getLastName(),
                manager.getAFM(),
                manager.getJobAddress()
                ));
    }

    @PostMapping("/{id}")
    public ResponseEntity<?> editManager(@PathVariable Long id, @RequestBody ManagerRequest request) throws Exception {
        managerService.updateManager(request, id);

        return ResponseEntity.ok(new MessageResponse("Manager profile updated successfully!"));
    }

    @PostMapping("/{id}/breeder")
    public ResponseEntity<?> updateBreeder(@PathVariable Long id, @RequestBody ManageBreeder request) throws Exception {
        breederSubmissionService.changeSubmissionsState(id, request);

        notifyFarmerForState(
                request.getState(),
                breederSubmissionService.getBreederSubmission(id).getFarmer().getPhoneNumber(),
                id
        );

        return ResponseEntity.ok(new MessageResponse("Application updated successfully!"));
    }

    @PostMapping("/{id}/submission")
    public ResponseEntity<?> updateSubmission(@PathVariable Long id, @RequestBody ManageSubmission request) throws Exception {
        submissionService.changeSubmissionsState(id, request);

        notifyFarmerForState(
                request.getState(),
                submissionService.getSubmission(id).getFarmer().getPhoneNumber(),
                id
        );

        return ResponseEntity.ok(new MessageResponse("Application updated successfully!"));
    }

    private void notifyFarmerForState(String state, Long phoneNumber, Long id) {
        String smsMessage;
        if (state.equals("ACCEPTED")) {
            smsMessage = " εγκρίθηκε!";
        }else {
            smsMessage = " απορρίφθηκε!";
        }
        Twilio.init(accountSID, authToken);
        Message.creator(
                        new com.twilio.type.PhoneNumber("+30" + phoneNumber),
                        new com.twilio.type.PhoneNumber("MYELGA"),
                        "Η αίτηση σας με αριθμό καταχώρησης " + id + smsMessage)
                .create();
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/new")
    public ResponseEntity<?> createManager(@Valid @RequestBody RegisterManagerRequest request) {
        System.out.println("****Start creating manager user.****" + request.getEmail());
        // Check if registration username and email are unique
        List<MessageResponse> message = new ArrayList<>();
        message.add(new MessageResponse(""));
        message.add(new MessageResponse(""));
        boolean badRequest = false;
        if (userRepository.existsByUsername(request.getUsername())) {
            badRequest = true;
            message.set(0, new MessageResponse("Το όνομα χρήστη χρησιμοποιείτε ήδη"));
        }
        if (userRepository.existsByEmail(request.getEmail())) {
            badRequest = true;
            message.set(1, new MessageResponse("Το email χρησιμοποιείτε ήδη"));
        }
        if (badRequest) {
            return ResponseEntity.badRequest().body(message);
        }

        Role role = roleRepository.findByName("ROLE_MANAGER")
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        User user = new User(
                request.getUsername(),
                request.getEmail(),
                bCryptPasswordEncoder.encode(request.getPassword())

        );
        user.setRoles(roles);

        User savedUser = userRepository.save(user);

        ElgaManager manager = new ElgaManager(
                request.getFirstName(),
                request.getLastName(),
                request.getAfm(),
                request.getJobAddress(),
                savedUser);

        managerService.saveManager(manager);

        return ResponseEntity.ok(new MessageResponse("User signed up successfully!"));
    }
}
