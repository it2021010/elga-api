package com.example.demo.controller;

import com.example.demo.entity.BreederSubmission;
import com.example.demo.entity.Farmer;
import com.example.demo.entity.Submission;
import com.example.demo.payload.request.FarmerProfileRequest;
import com.example.demo.payload.response.BreedersResponse;
import com.example.demo.payload.response.FarmerProfileResponse;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.payload.response.SubmissionsResponse;
import com.example.demo.service.FarmerService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/services/profile")
public class FarmerController {
    @Autowired
    private FarmerService farmerService;
    @Autowired
    private UserService userService;

    @GetMapping("{email}")
    public ResponseEntity<?> farmerInfo(@PathVariable String email) {
        Farmer farmer = farmerService.showFarmer(userService.getUserByEmail(email).getFarmer().getId());

        return ResponseEntity.ok(new FarmerProfileResponse(
                farmer.getFirstName(),
                farmer.getLastName(),
                farmer.getAddress(),
                farmer.getId(),
                farmer.getAFM(),
                farmer.getPhoneNumber()
        ));
    }

    @PostMapping("{farmer_id}")
    public ResponseEntity<?> editProfile(@PathVariable Long farmer_id, @RequestBody FarmerProfileRequest request) throws Exception {
        farmerService.editFarmer(request, farmer_id);

        return ResponseEntity.ok(new MessageResponse("Farmer's profile updated successfully!"));
    }

    @GetMapping("/{farmer_id}/my-submissions")
    public ResponseEntity<?> getMySubmissions(@PathVariable Long farmer_id) {
        List<Submission> submissionList = farmerService.getSubmissionsList(farmer_id);
        List<SubmissionsResponse> submissionsResponseList = new ArrayList<>();
        for (Submission submission: submissionList) {
            Double damages;
            if(submission.getDamages() == null) {
                damages = (double) -1;
            }else {
                damages = submission.getDamages().getDamagesSum();
            }
            submissionsResponseList.add(new SubmissionsResponse(submission.getId(), submission.getFieldArea(), submission.getAverageProduction(),
                        submission.getPricePerUnit(), submission.getDescription(), submission.getDamagePercentage(), submission.getState(),
                        submission.getCreationDate(), damages));
        }
        return ResponseEntity.ok(submissionsResponseList);
    }

    @GetMapping("/{farmer_id}/my-breeders")
    public ResponseEntity<?> getMyBreeders(@PathVariable Long farmer_id) {
        List<BreederSubmission> breederSubmissionList = farmerService.getBreederSubmissionsList(farmer_id);
        List<BreedersResponse> responseList = new ArrayList<>();
        for (BreederSubmission breeder: breederSubmissionList) {
            Double damages;
            if(breeder.getDamages() == null) {
                damages = (double) -1;
            }else {
                damages = breeder.getDamages().getDamagesSum();
            }
            responseList.add(new BreedersResponse(
                    breeder.getId(), breeder.getCreationDate(), breeder.getTotalAnimals(),
                    breeder.getState(), breeder.getResidualValue(), breeder.getPricePerUnit(),
                    breeder.getNumberOfKilledAnimals(), breeder.getDamageCoveragePercentage(),
                    breeder.getCompensationFactor(), damages));
        }

        return ResponseEntity.ok(responseList);
    }

}
