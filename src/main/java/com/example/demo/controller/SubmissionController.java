package com.example.demo.controller;

import com.example.demo.entity.BreederSubmission;
import com.example.demo.entity.Farmer;
import com.example.demo.entity.Submission;
import com.example.demo.entity.User;
import com.example.demo.payload.request.BreederRequest;
import com.example.demo.payload.request.SubmissionRequest;
import com.example.demo.payload.response.BreedersResponse;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.payload.response.SubmissionsResponse;
import com.example.demo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("/services")
public class SubmissionController {
    @Autowired
    private UserService userService;
    @Autowired
    private FarmerService farmerService;
    @Autowired
    private SubmissionService submissionService;
    @Autowired
    private BreederSubmissionService breederSubmissionService;
    @Autowired
    private FileService fileService;
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    @GetMapping("/farmer/{submission_id}")
    public ResponseEntity<?> getSubmission(@PathVariable Long submission_id) {
        Submission submission = submissionService.getSubmission(submission_id);
        Double damages;
        if(submission.getDamages() == null) {
            damages = (double) -1;
        }else {
            damages = submission.getDamages().getDamagesSum();
        }
        return ResponseEntity.ok(new SubmissionsResponse(
                submission.getId(),
                submission.getFieldArea(),
                submission.getAverageProduction(),
                submission.getPricePerUnit(),
                submission.getDescription(),
                submission.getDamagePercentage(),
                submission.getState(),
                submission.getCreationDate(),
                damages
        ));
    }
    @PostMapping("/farmer/{submission_id}")
    public ResponseEntity<?> editSubmission(@PathVariable Long submission_id, @RequestBody SubmissionRequest request) {
        submissionService.editSubmission(request, submission_id);
        return ResponseEntity.ok(new MessageResponse("Updated application successfully!"));
    }
    @DeleteMapping("/farmer/{submission_id}")
    public ResponseEntity<?> deleteSubmission(@PathVariable Long submission_id) {
        submissionService.deleteSubmission(submission_id);
        return ResponseEntity.ok(new MessageResponse("Application deleted successfully!"));
    }

    @GetMapping("/breeder/{submission_id}")
    public ResponseEntity<?> getBreeder(@PathVariable Long submission_id) {
        BreederSubmission breeder = breederSubmissionService.getBreederSubmission(submission_id);
        Double damages;
        if(breeder.getDamages() == null) {
            damages = (double) -1;
        }else {
            damages = breeder.getDamages().getDamagesSum();
        }
        return ResponseEntity.ok(new BreedersResponse(
                breeder.getId(),
                breeder.getCreationDate(),
                breeder.getTotalAnimals(),
                breeder.getState(),
                breeder.getResidualValue(),
                breeder.getPricePerUnit(),
                breeder.getNumberOfKilledAnimals(),
                breeder.getDamageCoveragePercentage(),
                breeder.getCompensationFactor(),
                damages
        ));
    }
    @PostMapping("/breeder/{submission_id}")
    public ResponseEntity<?> editBreeder(@PathVariable Long submission_id, @RequestBody BreederRequest request) {
        breederSubmissionService.editSubmission(request, submission_id);
        return ResponseEntity.ok(new MessageResponse("Updated application successfully!"));
    }
    @DeleteMapping("/breeder/{breeder_id}")
    public ResponseEntity<?> deleteBreederSubmission(@PathVariable Long breeder_id) {
        breederSubmissionService.deleteBreederSubmission(breeder_id);
        return ResponseEntity.ok(new MessageResponse("Application deleted successfully!"));
    }

    @PostMapping("/farmer/new/{farmer_email}")
    public ResponseEntity<?> saveFarmerSubmission(@PathVariable String farmer_email, @RequestBody SubmissionRequest request) throws Exception {
        User user = userService.getUserByEmail(farmer_email);
        Farmer farmer = user.getFarmer();

        Submission newSubmission = new Submission();
        newSubmission.setFieldArea(request.getFieldArea());
        newSubmission.setAverageProduction(request.getAverageProduction());
        newSubmission.setDescription(request.getDescription());
        newSubmission.setFarmer(farmer);
        newSubmission.setCreationDate(FORMAT.parse(FORMAT.format(new Date())));
        submissionService.saveSubmission(newSubmission);

        farmer.addSubmission(newSubmission);
        farmerService.saveFarmer(farmer);

        return ResponseEntity.ok(new MessageResponse("Created application successfully!"));
    }

    @PostMapping("/breeder/new/{farmer_email}")
    public ResponseEntity<?> saveBreederApplication(@PathVariable String farmer_email, @RequestBody BreederRequest request) throws Exception {
        User user = userService.getUserByEmail(farmer_email);
        Farmer farmer = user.getFarmer();

        BreederSubmission newBreeder = new BreederSubmission();
        newBreeder.setNumberOfKilledAnimals(request.getKilledAnimals());
        newBreeder.setTotalAnimals(request.getTotalAnimals());
        newBreeder.setPricePerUnit(request.getPricePerUnit());
        newBreeder.setFarmer(farmer);
        newBreeder.setCreationDate(FORMAT.parse(FORMAT.format(new Date())));
        breederSubmissionService.saveBreederSubmission(newBreeder);

        //farmer.addBreederSubmission(newBreeder);
        farmerService.saveFarmer(farmer);

        return ResponseEntity.ok(new MessageResponse("Created application successfully!"));
    }


    @GetMapping("/farmer/{id}/download")
    public ResponseEntity<?> getPdfOfSubmission(@PathVariable Long id) {
        byte[] fileData = fileService.getFile(submissionService.getSubmission(id).getFileApplication().getId()).getData();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("file.pdf", "filename.pdf");

        ResponseEntity<byte[]> response = new ResponseEntity<>(fileData, headers, HttpStatus.OK);

        return ResponseEntity.ok(response);
    }
    @GetMapping("/breeder/{id}/download")
    public ResponseEntity<?> getPdfOfBreeder(@PathVariable Long id) {
        byte[] fileData = fileService.getFile(breederSubmissionService.getBreederSubmission(id).getFileApplication().getId()).getData();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("file.pdf", "filename.pdf");

        ResponseEntity<byte[]> response = new ResponseEntity<>(fileData, headers, HttpStatus.OK);

        return ResponseEntity.ok(response);
    }
}
