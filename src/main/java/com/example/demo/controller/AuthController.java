package com.example.demo.controller;

import com.example.demo.config.JwtUtils;
import com.example.demo.entity.ElgaManager;
import com.example.demo.entity.Farmer;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.payload.UserDetailsImpl;
import com.example.demo.payload.request.*;
import com.example.demo.payload.response.JwtUserResponse;
import com.example.demo.payload.response.MessageResponse;
import com.example.demo.payload.response.PhoneNumberResponse;
import com.example.demo.payload.response.UsersListResponse;
import com.example.demo.repository.FarmerRepository;
import com.example.demo.repository.ManagerRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.FarmerService;
import com.example.demo.service.ManagerService;
import com.example.demo.service.UserService;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
//import com.twilio.rest.preview.accSecurity.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;
import com.twilio.rest.verify.v2.service.Verification;
import jakarta.annotation.PostConstruct;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private FarmerRepository farmerRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private ManagerService managerService;
    @Autowired
    private FarmerService farmerService;
    @Autowired
    private JavaMailSender javaMailSender;
    @Value("${twilio.account_sid}")
    private String accountSID;
    @Value("${twilio.auth_token}")
    private String authToken;
    @Value("${twilio.service_id}")
    private String verifySID;

    @PostMapping("/signin")
    public ResponseEntity<?> loginUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();

        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        System.out.println(Arrays.toString(roles.toArray()));
        if (roles.contains("ROLE_CITIZEN")) {
            // Get User Entity in order to get his phone and email
            User user = userRepository.findByUsername(loginRequest.getUsername()).get();


            // Send alert sms to user's phone number
            Twilio.init(accountSID, authToken);
            Message.creator(
                            new com.twilio.type.PhoneNumber("+30" + user.getFarmer().getPhoneNumber()),
                            new com.twilio.type.PhoneNumber("MYELGA"),
                            "Μόλις πραγματοποιήθηκε σύνδεση στην πλατφόρμα myElga.")
                    .create();

            // Send alert email to user's email
            SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(userDetails.getEmail());
            mailMessage.setSubject("Ειδοποίηση ασφαλείας");
            mailMessage.setText("Μόλις πραγματοποιήθηκε σύνδεση με το email σας στην πλατφόρμα myElga.");
            javaMailSender.send(mailMessage);
        }

        return ResponseEntity.ok(new JwtUserResponse(
                jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getEmail(),
                roles)
        );
    }

    @PostMapping("/signup")
    public ResponseEntity<?> register(@Valid @RequestBody RegisterRequest request) throws Exception {
        // Check if registration username and email are unique
        List<MessageResponse> message = new ArrayList<>();
        message.add(new MessageResponse(""));
        message.add(new MessageResponse(""));
        boolean badRequest = false;
        if (userRepository.existsByUsername(request.getUsername())) {
            badRequest = true;
            message.set(0, new MessageResponse("Το όνομα χρήστη χρησιμοποιείτε ήδη"));
        }
        if (userRepository.existsByEmail(request.getEmail())) {
            badRequest = true;
            message.set(1, new MessageResponse("Το email χρησιμοποιείτε ήδη"));
        }
        if (badRequest) {
            return ResponseEntity.badRequest().body(message);
        }

        // Create User and initialize Farmer for it and give [Role_Citizen]
        Role role = roleRepository.findByName("ROLE_CITIZEN")
                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
        Set<Role> roles = new HashSet<>();
        roles.add(role);

        User user = new User(
                request.getUsername(),
                request.getEmail(),
                bCryptPasswordEncoder.encode(request.getPassword())

        );
        user.setRoles(roles);
        // Initialize Farmer for that user and save User and Farmer
        userRepository.save(user);
        Farmer farmer = new Farmer(
                request.getFirstName(),
                request.getLastName(),
                request.getAfm(),
                request.getAddress(),
                request.getPhoneNumber()
        );
        farmerService.addFarmer(farmer, user.getId());

        return ResponseEntity.ok(new MessageResponse("User signed up successful!"));
    }

    @GetMapping("/{email}")
    public ResponseEntity<?> getAccount(@PathVariable String email) {
        boolean userExists = userRepository.existsByEmail(email);
        if (!userExists) {
            return ResponseEntity.badRequest().body(new MessageResponse("User with requested email does not exist!"));
        }else if (userRepository.findByEmail(email).getFarmer() == null) {
            return ResponseEntity.badRequest().body(new MessageResponse("User with this email cannot change password"));
        }
        Long returnId = userRepository.findByEmail(email).getFarmer().getPhoneNumber();

        return ResponseEntity.ok().body(new PhoneNumberResponse(returnId));
    }
    @PostMapping("/password")
    public ResponseEntity<?> changeAccountPassword(@RequestBody PasswordRequest request) {
        User user = userRepository.findByEmail(request.getEmail());
        System.out.println(request.getEmail() + "  " + request.getPassword());

        user.setPassword(bCryptPasswordEncoder.encode(request.getPassword()));

        userRepository.save(user);

        // Inform the user for password reset via email and sms
        Twilio.init(accountSID, authToken);
        Message.creator(
                        new com.twilio.type.PhoneNumber("+30" + user.getFarmer().getPhoneNumber()),
                        new com.twilio.type.PhoneNumber("MYELGA"),
                        "Μόλις πραγματοποιήθηκε αλλαγή συνθηματικού του λογαριασμού σας. Εάν δεν ήσασταν εσείς επικοινωνήστε με την πλατφόρμα.")
                .create();

        // Send alert email to user's email
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject("Ειδοποίηση ασφαλείας");
        mailMessage.setText("Μόλις πραγματοποιήθηκε αλλαγή συνθηματικού του λογαριασμού σας. Εάν δεν ήσασταν εσείς επικοινωνήστε με την πλατφόρμα.");
        javaMailSender.send(mailMessage);

        return ResponseEntity.ok().body(new MessageResponse("Success!"));
    }

    @PostMapping("/sendOtp")
    public ResponseEntity<?> sendVerificationCode(@Valid @RequestBody PhoneEmailRequest request) {
        Twilio.init(accountSID, authToken);
        System.out.println(request.getEmail() + request.getPhoneNumber());
        Verification verification =
                Verification.creator(
                        verifySID,
                        "+30" + request.getPhoneNumber(),
                        "sms")
                        .create();

       return ResponseEntity.ok().body(new MessageResponse("Sent verification code"));
    }
    @PostMapping("/validateOtp")
    public ResponseEntity<?> validateVerificationCode(@RequestBody VerificationRequest request) {
        //Twilio.init(accountSID, authToken);
        System.out.println(request.getPhoneNumber() + request.getVerificationCode());

        VerificationCheck verificationCheck =
                    VerificationCheck.creator(verifySID)
                            .setTo("+30" + request.getPhoneNumber())
                            .setCode(request.getVerificationCode())
                            .create();
        if (!verificationCheck.getValid()) {
            System.out.println("Verification failed =====> ");
            return ResponseEntity.badRequest().body(new MessageResponse("Verification failed."));
        }

        System.out.println("Verified");
        return ResponseEntity.ok().body(new MessageResponse("Verification success!"));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{email}")
    public ResponseEntity<?> deleteUser(@PathVariable String email){
        Long userId;
        try {
            userId = userRepository.findByEmail(email).getId();
        }catch (Exception e) {
            return ResponseEntity.badRequest().body("Can't find user");
        }
        System.out.println("About to delete user with id ---> " + userId);
        userService.deleteUserById(userId);
        System.out.println("Deleted user with id ---> " + userId);
        return ResponseEntity.ok().body(new MessageResponse("User deleted!"));
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("")
    public ResponseEntity<?> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UsersListResponse> responseList = new ArrayList<>();
        for (User u : users) {
            Set<String> roleSet = new HashSet<>();
            for (Role role : u.getRoles()) {
                roleSet.add(String.valueOf(role));
            }
            responseList.add(new UsersListResponse(u.getId(), u.getUsername(), u.getEmail(), u.getPassword(), roleSet));
        }
        return ResponseEntity.ok().body(responseList);
    }



    /*@PostConstruct
    public void createFarmer() throws Exception {
        //User user = new User("myFarmer", "myFarmer@hua.gr", bCryptPasswordEncoder.encode("pass"));
        User user = new User();
        user.setUsername("myFarmer");
        user.setPassword(bCryptPasswordEncoder.encode("pass"));
        user.setEmail("myFarmer@hua.gr");
        user.setRoles(Set.of(roleRepository.findByName("ROLE_CITIZEN").get()));
        userRepository.save(user);

        farmerService.addFarmer(new Farmer(), user.getId());


    }*/
    /*@PostConstruct
    public void createManager() throws Exception {
        User user = new User("it2021010", "2021010@hua.gr", bCryptPasswordEncoder.encode("pass"));
        Long savedId = userService.saveUserAsManager(user);

        ElgaManager manager = new ElgaManager();
        managerService.saveManager(manager, savedId);
    }*/
}
