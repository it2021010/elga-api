package com.example.demo.config;

import com.example.demo.entity.User;
import com.example.demo.service.InitializeAppData;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;


@Component
public class InitializeDatabase {
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Autowired
    private InitializeAppData initializeAppData;

    @PostConstruct
    public void setup() {
        this.createAdmin();
    }

    public void createAdmin() {
        User user = new User();
        user.setUsername("Admin");
        user.setEmail("admin@hua.gr");
        user.setPassword(bCryptPasswordEncoder.encode("ADMINPASS123!"));

        this.initializeAppData.saveUser(user, "ROLE_ADMIN");
    }
}
