package com.example.demo.service;

import com.example.demo.entity.FileApplication;
import com.example.demo.payload.PdfFileFields;
import com.example.demo.repository.FileApplicationRepository;
import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Map;

@Service
public class FileService {
    private static final String DEST = "C:/Users/c_apo/Desktop/Backend myElga/demo/pdfSubmissions/tempFilledPdf.pdf"; //fixme
    private static final Path SUBMISSION_PATH = Paths.get("C:/Users/c_apo/Desktop/Backend myElga/demo/pdfSubmissions/farmerApplication.pdf"); //fixme
    private static final Path BREEDER_PATH = Paths.get("C:/Users/c_apo/Desktop/Backend myElga/demo/pdfSubmissions/breederApplication.pdf"); //fixme

    @Autowired
    private FileApplicationRepository fileRepository;

    @Transactional
    public FileApplication getFile(Long id) {
        return fileRepository.findById(id).get();
    }

    @Transactional
    public FileApplication storeFile(FileApplication file) {
        return fileRepository.save(file);
    }

    @Transactional
    public boolean generatePdfFile(PdfFileFields givenFields) throws IOException {
        File file;
        if (givenFields.getApplicationType().equals("submission")) {
            file = new File(String.valueOf(SUBMISSION_PATH));
        }else if (givenFields.getApplicationType().equals("breeder")) {
            file = new File(String.valueOf(BREEDER_PATH));
        }else {
            return false;
        }
        file.setWritable(false);

        PdfDocument pdfDoc = new PdfDocument(new PdfReader(file), new PdfWriter(DEST));
        PdfAcroForm form = PdfAcroForm.getAcroForm(pdfDoc, true);

        Map<String, PdfFormField> fields = form.getAllFormFields();
        fields.get("id").setValue(Long.toString(givenFields.getId()));
        fields.get("name").setValue(givenFields.getFirstName());
        fields.get("lastName").setValue(givenFields.getLastName());
        fields.get("address").setValue(givenFields.getAddress());
        fields.get("phone").setValue(Long.toString(givenFields.getPhoneNumber()));
        fields.get("afm").setValue(Long.toString(givenFields.getAfm()));
        if (givenFields.getApplicationType().equals("submission")) {
            fields.get("field").setValue(Integer.toString(givenFields.getFieldArea()));
            fields.get("average").setValue(Float.toString(givenFields.getAverageProduction()));
        }else if (givenFields.getApplicationType().equals("breeder")) {
            fields.get("killed").setValue(Integer.toString(givenFields.getKilledAnimals()));
            fields.get("total").setValue(Integer.toString(givenFields.getTotalAnimals()));
            fields.get("factor").setValue(Float.toString(givenFields.getCompensationFactor()));
            fields.get("residual").setValue(String.valueOf(givenFields.getResidualValue()));
        }
        fields.get("percentage").setValue(Float.toString(givenFields.getDamagePercentage()));
        fields.get("price").setValue(Double.toString(givenFields.getPricePerUnit()));
        fields.get("date").setValue(givenFields.getCreationDate());
        fields.get("damages").setValue(givenFields.getDamages().toString() + "€");

        form.flattenFields();

        pdfDoc.close();
        return true;
    }
}
