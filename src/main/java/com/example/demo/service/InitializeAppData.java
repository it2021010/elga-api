package com.example.demo.service;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.ManagerRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.PostLoad;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class InitializeAppData {

    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public void saveUser(User user, String roleToAdd) {
        Role adminRole = this.roleRepository.findByName(roleToAdd).orElseThrow();
        Set<Role> roles = new HashSet<>();
        roles.add(adminRole);

        user.setRoles(roles);

        this.userRepository.save(user);
    }

}
