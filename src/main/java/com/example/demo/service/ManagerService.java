package com.example.demo.service;

import com.example.demo.entity.ElgaManager;
import com.example.demo.entity.User;
import com.example.demo.payload.request.ManagerRequest;
import com.example.demo.repository.ManagerRepository;
import com.example.demo.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ManagerService {
    @Autowired
    private ManagerRepository managerRepository;
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public void saveManager(ElgaManager manager) {
        managerRepository.save(manager);
    }

    @Transactional
    public ElgaManager getManager(Long id) {
        return managerRepository.findById(id).get();
    }

    @Transactional
    public ElgaManager updateManager(ManagerRequest manager, Long managerId) throws Exception {
        Optional<ElgaManager> opt = managerRepository.findById(managerId);
        if (opt.isEmpty()) {
            throw new Exception("cannot update farmer");
        }else {
            opt.get().setFirstName(manager.getFirstName());
            opt.get().setLastName(manager.getLastName());
            opt.get().setAFM(manager.getAfm());
            opt.get().setJobAddress(manager.getJobAddress());
            //opt.get().setSubmissions(manager.getSubmissions());
            return managerRepository.save(opt.get());
        }
    }
}
