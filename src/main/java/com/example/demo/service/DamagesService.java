package com.example.demo.service;

import com.example.demo.entity.Damages;
import com.example.demo.repository.DamagesRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DamagesService {

    @Autowired
    private DamagesRepository damagesRepository;

    @Transactional
    public Damages saveDamages(Damages damages) {
        return damagesRepository.save(damages);
    }
    @Transactional
    public Damages getDamages(Long id) {
        return damagesRepository.findById(id).get();
    }
}
