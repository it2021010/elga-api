package com.example.demo.service;

import com.example.demo.entity.BreederSubmission;
import com.example.demo.entity.Damages;
import com.example.demo.entity.FileApplication;
import com.example.demo.entity.Submission;
import com.example.demo.payload.PdfFileFields;
import com.example.demo.payload.request.BreederRequest;
import com.example.demo.payload.request.ManageBreeder;
import com.example.demo.repository.BreederSubmissionRepository;
import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.forms.fields.PdfFormField;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class BreederSubmissionService {
    private static final String DEST = "C:/Users/c_apo/Desktop/Backend myElga/demo/pdfSubmissions/tempFilledPdf.pdf"; //fixme filename
    @Autowired
    private BreederSubmissionRepository breederSubmissionRepository;
    @Autowired
    private FileService fileService;
    @Autowired
    private DamagesService damagesService;

    @Transactional
    public void saveBreederSubmission(BreederSubmission breederSubmission) {
        breederSubmissionRepository.save(breederSubmission);
    }

    @Transactional
    public BreederSubmission editSubmission(BreederRequest breederSubmission, Long submission_id){
        BreederSubmission savedSubmission = breederSubmissionRepository.findById(submission_id).get();
        if (breederSubmission.getKilledAnimals() != 0) {
            savedSubmission.setNumberOfKilledAnimals(breederSubmission.getKilledAnimals());
        }
        if (breederSubmission.getTotalAnimals() != 0) {
            savedSubmission.setTotalAnimals(breederSubmission.getTotalAnimals());
        }
        if (breederSubmission.getPricePerUnit() != null) {
            savedSubmission.setPricePerUnit(breederSubmission.getPricePerUnit());
        }

        return breederSubmissionRepository.save(savedSubmission);
    }

    @Transactional
    public List<BreederSubmission> getAllSubmissions() {
        return breederSubmissionRepository.findAll();
    }

    @Transactional
    public void deleteBreederSubmission(Long breeder_id) {
        breederSubmissionRepository.deleteById(breeder_id);
    }

    @Transactional
    public BreederSubmission getBreederSubmission(Long submissionId) {
        return breederSubmissionRepository.findById(submissionId).get();
    }

    @Transactional
    public void changeSubmissionsState(Long id, ManageBreeder request) throws Exception {
        Optional<BreederSubmission> opt = breederSubmissionRepository.findById(id);
        if (opt.isEmpty()) {
            throw new Exception("cannot update breeder application");
        }else {
            if (request.getState().equals("REJECTED")) {
                opt.get().setState("REJECTED");
                breederSubmissionRepository.save(opt.get());
            }else if (request.getState().equals("ACCEPTED")) {
                opt.get().setState(request.getState());
                opt.get().setDamageCoveragePercentage(request.getDamagePercentage());
                opt.get().setCompensationFactor(request.getCompensationFactor());
                opt.get().setResidualValue(request.getResidualValue());
                BreederSubmission returnSubmission = breederSubmissionRepository.save(opt.get());

                Damages savedDamages = damagesService.saveDamages(new Damages(returnSubmission.calculateCompensation()));

                returnSubmission.setDamages(savedDamages);
                breederSubmissionRepository.save(returnSubmission);


                // Create PDF File and store it
                //fixme lock
                fileService.generatePdfFile(new PdfFileFields(
                        "breeder", returnSubmission.getId(), returnSubmission.getFarmer().getFirstName(),
                        returnSubmission.getFarmer().getLastName(), returnSubmission.getFarmer().getAddress(),
                        returnSubmission.getFarmer().getPhoneNumber(), returnSubmission.getFarmer().getAFM(),
                        returnSubmission.getNumberOfKilledAnimals(), returnSubmission.getTotalAnimals(),
                        returnSubmission.getCompensationFactor(), returnSubmission.getResidualValue(),
                        returnSubmission.getPricePerUnit(), returnSubmission.getDamageCoveragePercentage(),
                        returnSubmission.getCreationDate(), returnSubmission.getDamages().getDamagesSum()
                ));
                System.out.println("Created finally successfully!");
                // Store file to database
                String filename = "application" + returnSubmission.getId();
                FileApplication fileApplication = new FileApplication(Files.readAllBytes(Paths.get(DEST)), filename);
                FileApplication storedFile = fileService.storeFile(fileApplication);
                //fixme unlock

                BreederSubmission breederSubmission = breederSubmissionRepository.findById(returnSubmission.getId()).get();
                breederSubmission.setFileApplication(storedFile);
                breederSubmissionRepository.save(breederSubmission);
            }
        }
    }

}
