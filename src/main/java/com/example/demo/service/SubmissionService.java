package com.example.demo.service;

import com.example.demo.entity.*;
import com.example.demo.payload.PdfFileFields;
import com.example.demo.payload.request.ManageSubmission;
import com.example.demo.payload.request.SubmissionRequest;
import com.example.demo.repository.SubmissionRepository;
import com.itextpdf.forms.fields.PdfFormField;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.forms.PdfAcroForm;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;

import java.io.File;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class SubmissionService {
    private static final String DEST = "C:/Users/c_apo/Desktop/Backend myElga/demo/pdfSubmissions/tempFilledPdf.pdf"; //fixme
    @Autowired
    private SubmissionRepository submissionRepository;
    @Autowired
    private FileService fileService;
    @Autowired
    private DamagesService damagesService;

    @Transactional
    public void saveSubmission(Submission submission) {
        submissionRepository.save(submission);
    }

    @Transactional
    public Submission editSubmission(SubmissionRequest submission, Long submission_id) {
        Submission savedSubmission = submissionRepository.findById(submission_id).get();

        if (submission.getFieldArea() != 0) {
            savedSubmission.setFieldArea(submission.getFieldArea());
        }
        if (submission.getAverageProduction() != 0) {
            savedSubmission.setAverageProduction(submission.getAverageProduction());
        }
        if (!submission.getDescription().isEmpty()) {
            savedSubmission.setDescription(submission.getDescription());
        }

        return submissionRepository.save(savedSubmission);
    }

    @Transactional
    public List<Submission> getAllSubmissions() {
        return submissionRepository.findAll();
    }

    @Transactional
    public void deleteSubmission(Long submissionId) {
        submissionRepository.deleteById(submissionId);
    }

    @Transactional
    public Submission getSubmission(Long submissionId) {
        return submissionRepository.findById(submissionId).get();
    }


    @Transactional
    public void changeSubmissionsState(Long id, ManageSubmission request) throws Exception {
        Optional<Submission> opt = submissionRepository.findById(id);
        if (opt.isEmpty()) {
            throw new Exception("cannot update farmer");
        }else {
            if (request.getState().equals("REJECTED")) {
                opt.get().setState("REJECTED");
                submissionRepository.save(opt.get());
            }else if(request.getState().equals("ACCEPTED")) {
                opt.get().setState("ACCEPTED");
                opt.get().setDamagePercentage(request.getDamagePercentage());
                opt.get().setPricePerProductUnit(request.getPricePerUnit());
                Submission returnSubmission = submissionRepository.save(opt.get());

                Damages savedDamages = damagesService.saveDamages(new Damages(returnSubmission.calculateCompensation()));

                returnSubmission.setDamages(savedDamages);
                submissionRepository.save(returnSubmission);


                // Create PDF File and store it
                //fixme lock
                fileService.generatePdfFile(new PdfFileFields(
                        "submission", returnSubmission.getId(), returnSubmission.getFarmer().getFirstName(),
                        returnSubmission.getFarmer().getLastName(), returnSubmission.getFarmer().getAddress(),
                        returnSubmission.getFarmer().getPhoneNumber(), returnSubmission.getFarmer().getAFM(),
                        returnSubmission.getFieldArea(), returnSubmission.getAverageProduction(),
                        returnSubmission.getPricePerUnit(), returnSubmission.getDamagePercentage(),
                        returnSubmission.getCreationDate(), returnSubmission.getDamages().getDamagesSum()
                        ));

                // Store file to database
                String filename = "application" + returnSubmission.getId();
                FileApplication fileApplication = new FileApplication(Files.readAllBytes(Paths.get(DEST)), filename);
                FileApplication storedFile = fileService.storeFile(fileApplication);
                //fixme unlock

                // Set created file to Submission and store it to database
                Submission submission = submissionRepository.findById(returnSubmission.getId()).get();
                submission.setFileApplication(storedFile);
            }
        }
    }

}
