package com.example.demo.service;

import com.example.demo.entity.BreederSubmission;
import com.example.demo.entity.Farmer;
import com.example.demo.entity.Submission;
import com.example.demo.entity.User;
import com.example.demo.payload.request.FarmerProfileRequest;
import com.example.demo.repository.FarmerRepository;
import com.example.demo.repository.UserRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FarmerService {

    @Autowired
    private FarmerRepository farmerRepository;
    @Autowired
    private UserRepository userRepository;

    @Transactional
    public Long addFarmer(Farmer farmer, Long user_id) throws Exception{
        Optional<User> user = userRepository.findById(user_id);
        if (user.isEmpty()) {
            throw new Exception("cant add farmer");
        }else {
            farmer.setUser(user.get());
            farmerRepository.save(farmer);
            return farmer.getId();
        }
    }

    @Transactional
    public Farmer showFarmer(Long farmer_id) {
        return farmerRepository.findById(farmer_id).get();
    }
    @Transactional
    public Farmer editFarmer(FarmerProfileRequest farmer, Long farmer_id) throws Exception{
        Optional<Farmer> opt = farmerRepository.findById(farmer_id);
        if (opt.isEmpty()) {
            throw new Exception("cannot update farmer");
        }else {
            if (!farmer.getAddress().isEmpty()) {
                opt.get().setAddress(farmer.getAddress());
            }
            if (!farmer.getPhoneNumber().isEmpty()) {
                opt.get().setPhoneNumber(Long.valueOf(farmer.getPhoneNumber()));
            }

            return farmerRepository.save(opt.get());
        }
    }
    @Transactional
    public Farmer saveFarmer(Farmer farmer) {
        return farmerRepository.save(farmer);
    }
    @Transactional
    public List<Submission> getSubmissionsList(Long farmerId) {
        return farmerRepository.findById(farmerId).get().getSubmissions();
    }

    @Transactional
    public List<BreederSubmission> getBreederSubmissionsList(Long farmerId) {
        return farmerRepository.findById(farmerId).get().getBreederSubmissions();
    }
    
}
