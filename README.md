# Build Instructions
This project was build with Maven tool.
<kbd>mvn compile</kbd>
<kbd>mvn package</kbd>

# Run project
You can start the server with this command:
<kbd> java -jar .\target\demo-0.0.1-SNAPSHOT.jar </kbd>